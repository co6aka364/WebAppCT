import React from "react";
import ReactDOM from "react-dom";
import {Provider} from "react-redux";
import {browserHistory, IndexRedirect, Route, Router} from "react-router";
import {syncHistoryWithStore} from "react-router-redux";
import configureStore from "./redux/store/configureStore";


/*COMPONENTS TO PATH*/
import Editor from "./containers/editor/editor.jsx";
// import Login from './components/auth/login.jsx';
// import Registration from './components/auth/registration.jsx';
import NotFound from "./components/not-found/not-found.jsx";
import "react-select/dist/react-select.css";
import "./public/scss/index.scss";
/*COMPONENTS TO PATH*/
const store = configureStore();
const history = syncHistoryWithStore(browserHistory, store);


const requireAuth = (nextState, replace) => {
    // if (!auth.isHasToken()) {
    //     replace({pathname: '/login', query: {return_to: nextState.location.pathname}});
    //
    // }
};

ReactDOM.render(
    <Provider store={store}>
        <Router history={history}>
            <Route path="/">
                <IndexRedirect to="/editor"/>
                <Route path="/editor" component={Editor} onEnter={requireAuth}/>
            </Route>
            {/*<Route path="/login" component={Login}/>*/}
            {/*<Route path="/registration" component={Registration}/>*/}
            <Route path="*" component={NotFound}/>
        </Router>
    </Provider>,
    document.getElementById("body")
);