import {
    REBUILD_TREE,
    SELECT_ELEMENT,
    DESELECT_ELEMENT,
    CHANGE_SCALE,
    DELETE_ELEMENT
} from '../constants/element';
import $ from 'jquery';
import _ from 'underscore';

export function rebuildTree() {
    let elements = [];
    $('[data-wact-element]').each((ii, e) => {
        let $e = $(e);
        elements.push({
            id: $e.attr('id'),
            children: [],
            parentId: $e.attr('data-parent'),
            name: $e.attr('data-name')
        });
    });
    let buildTree = (array, parent, tree) => {
        'use strict';

        tree = typeof tree !== 'undefined' ? tree : [];
        parent = typeof parent !== 'undefined' ? parent : {id: null};

        let children = _.filter(array, function (child) {
            return child.parentId == parent.id;
        });

        if (!_.isEmpty(children)) {
            if (parent.id === null) {
                tree = children;
            } else {
                parent['children'] = children;
            }
            _.each(children, function (child) {
                buildTree(array, child)
            });
        }

        return tree;
    };

    let tree = buildTree(elements);
    return (dispatch) => {
        "use strict";

        dispatch({
            type: REBUILD_TREE,
            payload: tree
        })
    }
}

export function selectElement(elementId) {
    $('[data-element-menu]').css('display', 'none');
    $('[data-element-menu]').click(null);
    if (!elementId) {
        return (dispatch) => {
            dispatch({
                type: DESELECT_ELEMENT,
                payload: null
            })
        }
    }
    $(`[data-selected="true"]`).attr('data-selected', '');
    $(`div#${elementId}`).attr('data-selected', true);

    return (dispatch) => {
        dispatch({
            type: SELECT_ELEMENT,
            payload: {
                id: elementId,
                element: $(`div#${elementId}`),
                type: $(`div#${elementId}`).attr('data-element-type')
            }
        })
    }
}

export function changeScale(scale) {
    if (!scale) {
        scale = 1
    } else {
        scale = scale / 100;
    }
    return (dispatch) => {
        "use strict";
        dispatch({
            type: CHANGE_SCALE,
            payload: scale
        })
    }
}

export function removeElement(element) {
    "use strict";

    if (element) {
        $(`#${element.id}`).remove();
    }
    return (dispath) => {
        dispath({
            type: DESELECT_ELEMENT,
            payload: null
        })
    }
}