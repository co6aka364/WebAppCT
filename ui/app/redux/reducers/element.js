import {
    REBUILD_TREE,
    SELECT_ELEMENT,
    DESELECT_ELEMENT,
    DELETE_ELEMENT,
    CHANGE_SCALE
} from '../constants/element';

const initialState = {
    tree: [],
    selected: {},
    scale: 1
};

export default function element(state = initialState, action) {
    switch (action.type) {
        case REBUILD_TREE:
            return {
                ...state,
                tree: action.payload
            };
        case SELECT_ELEMENT:
            return {
                ...state,
                selected: action.payload
            };
        case DESELECT_ELEMENT:
            return {
                ...state,
                selected: action.payload
            };
        case CHANGE_SCALE:
            return {
                ...state,
                scale: action.payload
            };
        default:
            return state;
    }
}