import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';
import element from './element';

export default combineReducers({
    element,
    routing: routerReducer
})