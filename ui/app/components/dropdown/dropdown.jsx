import React, {Component} from 'react';
import Util from '../../public/js/util';

import './dropdown.scss';

class Dropdown extends Component {
    constructor(props) {
        super(props);
        this.state = {
            guid: Util.guid(),
            isOpen: false,
            timeout: null
        };
    }

    onClick = (e) => {
        let bodyRect = document.body.getBoundingClientRect();
        let btnRect = e.target.getBoundingClientRect();
        let btnOffsetTop = btnRect.top - bodyRect.top;
        let btnOffsetLeft = btnRect.left - bodyRect.left;
        let scroll = document.documentElement.scrollTop || document.body.scrollTop;

        if (this.props.preOpenFunc && typeof this.props.preOpenFunc === 'function') {
            this.props.preOpenFunc();
        }

        this.setState({
            isOpen: true,
            left: btnOffsetLeft + 'px',
            // top: btnOffsetTop - scroll + 'px'
        });
    };

    buttonOnMouseLeave = () => {
        this.setState({
            timeout: setTimeout(() => {
                this.setState({isOpen: false});
            }, 200)
        });
    };

    menuOnMouseEnter = () => {
        setTimeout(() => {
            if (this.state.timeout) {
                clearTimeout(this.state.timeout);
            }
        }, 10)
    };

    menuOnMouseLeave = () => {
        this.setState({
            timeout: setTimeout(() => {
                this.setState({isOpen: false});
            }, 200)
        });
    };

    onClickItem = (props) => {
        if (props.onClick && typeof props.onClick === 'function') {
            props.onClick();
        }
        if (this.props.closeByClickItem) {
            this.setState({isOpen: false});
        }
    };

    render() {
        return (
            <span>
            <button className={this.props.buttonClass}
                    onClick={this.onClick.bind(this)}
                    onMouseLeave={this.buttonOnMouseLeave}
                    disabled={this.props.disabled}
            >
                {this.props.title}
            </button>
                {
                    this.state.isOpen
                        ?
                        <div style={Object.assign(this.props.menuStyle, {
                            left: this.state.left,
                            // top: this.state.top
                        })}
                             onMouseEnter={this.menuOnMouseEnter}
                             onMouseLeave={this.menuOnMouseLeave}
                             className="dropdown-menu">
                            {
                                this.props.children
                                    ? this.props.children.map((e, ii) => {
                                    console.log(e.props.onClick);
                                    return (
                                        <div key={ii}
                                             className="dropdown-item"
                                             onClick={this.onClickItem.bind(this, e.props)}>
                                            {e}
                                        </div>
                                    );
                                })
                                    : ''
                            }
                        </div>
                        : ''
                }
        </span>
        )
    }
}

Dropdown.PropTypes = {
    title: React.PropTypes.string.isRequired,
    buttonClass: React.PropTypes.string,
    menuStyle: React.PropTypes.object,
    closeByClickItem: React.PropTypes.bool,
    disabled: React.PropTypes.bool,
    preOpenFunc: React.PropTypes.func
};

Dropdown.defaultProps = {
    // selectedOptions: []
    buttonClass: 'btn btn_size_12',
    menuStyle: {},
    closeByClickItem: true,
    preOpenFunc: null,
    disabled: false
};

export default Dropdown;