import React, {Component} from 'react';

import './dropdown.scss';

class DropdownItem extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    onClickItem = () => {
        if (this.props.closeByClickItem) {
            this.props.closeByClickItem();
        }
    };

    render() {
        return (
            <div className="dropdown-item" onClick={this.onClickItem}>
                {this.props.children}
            </div>
        );
    }
}

export default DropdownItem;