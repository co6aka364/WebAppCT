import React, {Component} from "react";
import Window from '../window/window.jsx';
import PropertySet from '../property-set/property-set.jsx';
import $ from 'jquery';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as elementActions from '../../../redux/actions/element';

import "./property-window.scss";

class PropertyWindow extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    onNameChange = () => {
        this.props.elementActions.rebuildTree();
    };

    render() {
        return (
            <Window title="Property"
                    positionX="right"
                    positionY="bottom"
                    windowId="property-window"
                    resizable={false}
                    draggable={true}
            >
                {
                    this.props.selectedElement && this.props.selectedElement.id
                        ? <PropertySet
                        type={this.props.selectedElement.type}
                        element={this.props.selectedElement.element}
                        onNameChange={this.onNameChange}
                    />
                        : ''
                }
            </Window>
        )
    }
}


function mapStateToProps(state) {
    return {
        selectedElement: state.element.selected
    }
}

function mapDispatchToProps(dispatch) {
    return {
        elementActions: bindActionCreators(elementActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PropertyWindow);