import React, {Component} from 'react';

import './tree.scss';
import $ from 'jquery';

class Node extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    toggleCaret = () => {
        this.setState({open: !this.state.open});
    };

    componentWillMount() {
        this.setState({open: this.props.open})
    }

    render() {
        let hasChildren = this.props.node.children
            && this.props.node.children.length > 0;
        let isOpen = this.state.open;
        return (
            <li key={this.props.node.id} className={hasChildren ? 'no-padding' : ''}>
                <span className={hasChildren ? 'caret' : ''}
                      open={isOpen}
                      onClick={this.toggleCaret}
                ></span>
                <span className="item"
                      onClick={this.props.onNodeClick.bind(this, this.props.node.id)}
                      onMouseEnter={this.props.onNodeEnter.bind(this, this.props.node.id)}
                      onMouseLeave={this.props.onNodeLeave.bind(this, this.props.node.id)}
                >{this.props.node.name ? this.props.node.name : 'no-name'}</span>

                {
                    hasChildren && isOpen
                        ?
                        <ul>
                            {
                                this.props.node.children.map((child, ii) => {
                                    return <Node key={ii}
                                                 onNodeEnter={this.props.onNodeEnter}
                                                 onNodeClick={this.props.onNodeClick}
                                                 onNodeLeave={this.props.onNodeLeave}
                                                 node={child} open="false"/>
                                })
                            }
                        </ul>
                        : ''
                }
            </li>);
    }
}

Node.defaultProps = {
    open: false
};

Node.PropTypes = {
    node: React.PropTypes.object.isRequired,
    open: React.PropTypes.bool
};

export default Node;