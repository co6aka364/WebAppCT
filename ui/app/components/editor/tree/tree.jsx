import React, {Component} from 'react';
import Node from './tree-node.jsx';
import './tree.scss';
import $ from 'jquery';

class Tree extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {

        return <div className="tree">
            <ul >
                {
                    this.props.nodes.map((node) => {
                        return <Node key={-1}
                                     onNodeClick={this.props.onNodeClick}
                                     onNodeEnter={this.props.onNodeEnter}
                                     onNodeLeave={this.props.onNodeLeave}
                                     node={node} open={false}/>;
                    })
                }
            </ul>
        </div>
    }
}

Tree.PropTypes = {
    nodes: React.PropTypes.array.isRequired
};

export default Tree;