import React, {Component} from "react";
import $ from 'jquery';
import * as propertySets from '../../../public/dictionary/property-set';
import * as constants from '../../../public/dictionary/constants';

class PropertySet extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    componentWillMount() {
        let element = this.props.element.children().length > 0
            ? this.props.element.children()[2] : this.props.element[0];
        this.setState({
            style: window.getComputedStyle(element),
            pageName: this.props.element.attr('data-name'),
            innerText: element.innerText || '',
            href: element.getAttribute('href') || ''
        })
    }

    componentWillReceiveProps(nextProps) {
        let element = nextProps.element.children().length > 0 && !nextProps.element.hasClass('workplace-container')
            ? nextProps.element.children()[2] : nextProps.element[0];
        this.setState({
            style: window.getComputedStyle(element),
            pageName: nextProps.element.attr('data-name'),
            innerText: element.innerText || '',
            href: element.getAttribute('href') || ''
        })
    }

    onChangeProperty = (prop, event) => {
        if (prop.toString().toUpperCase() === 'INNERTEXT') {
            this.setState({...this.state, innerText: event.target.value});
            this.props.element.children()[2].innerText = event.target.value;
        }
        if (prop.toString().toUpperCase() === 'HREF') {
            this.props.element.children()[2].setAttribute('href', event.target.value);
            this.setState({...this.state, href: event.target.value})
        }

        this.setState({style: {...this.state.style, [prop]: event.target.value}});
        this.props.element.children().length > 0 && !this.props.element.hasClass('workplace-container')
            ?
            this.props.element.children()[2].style[prop] = event.target.value
            :
            this.props.element[0].style[prop] = event.target.value
    };

    render() {
        let elements = [];
        if (this.props.type && propertySets[this.props.type.toLowerCase()]) {
            elements = elements.concat(propertySets[this.props.type.toLowerCase()]);
        } else {
            elements = elements.concat(propertySets[constants.elements.block.toLowerCase()]);
        }
        elements = elements.concat(propertySets.extended);

        let elementsHTML = [];
        elementsHTML.push(
            <div className="d-row" key={-1}>
                <div className="d-cell d-cell_width_50 word_break"><span>Name</span></div>
                <div className="d-cell align-right">
                    <input className="property-input width_100per"
                           type="text"
                           value={this.state.pageName}
                           onChange={(e) => {
                               this.setState({pageName: e.target.value});
                               this.props.element.attr('data-name', e.target.value);
                               this.props.onNameChange();
                           }}
                    />
                </div>
            </div>
        );

        //render elements
        elementsHTML = elementsHTML.concat(elements.map((e, ii) => {
            let isNotStyle = e.property.toLowerCase() === 'href' || e.property.toLowerCase() === 'innertext';
            // console.log(e);
            return <div className="d-row" key={ii}>
                <div className="d-cell d-cell_width_50 word_break"><span>{e.name ? e.name : e.property}</span></div>
                <div className="d-cell align-right">
                    <input className="property-input width_100per"
                           type="text"
                           value={!isNotStyle ? this.state.style[e.property] : this.state[e.property]}
                           onChange={this.onChangeProperty.bind(this, e.property)}
                    />
                </div>
            </div>
        }));

        return (
            <div className="d-table">
                {elementsHTML}
            </div>
        )
    }
}

PropertySet.PropTypes = {
    // set: React.PropTypes.array.isRequired(React.PropTypes.object),
    type: React.PropTypes.string.isRequired,
    element: React.PropTypes.object.isRequired
};

export default PropertySet;