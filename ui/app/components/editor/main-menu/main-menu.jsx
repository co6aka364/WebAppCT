import React, {Component} from "react";
import $ from 'jquery';
import moment from 'moment';
import Dropdown from '../../dropdown/dropdown.jsx';
import Modal from 'react-modal';

import functionalHandler from '../hoc/functional-handlers.jsx';

import "./main-menu.scss";
// import DropdownItem from "../../dropdown/dropdown-item";

class MainMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isEntered: false,
            isOpenWindow: true,
            isHoverWindow: false,
            windows: [],
            modalLoad: {
                isOpen: false,
                title: '',
                content: []
            },
            modalSave: {
                isOpen: false,
                content: ''
            }
        }
    }

    handleName = (event) => {
        this.setState({modalSave: {...this.state.modalSave, content: event.target.value}})
    };

    openModal = () => {
        this.setState({modalLoad: {...this.state.modalLoad, isOpen: true}});
    };

    closeModal = () => this.setState({modalLoad: {isOpen: false, content: []}});

    openModalSave = () => {
        let $e = $('.workplace');
        let projectId = $e.attr('data-project-id');
        let projectName = $e.attr('data-project-name');

        if (projectName && projectId) {
            this.props.menuFunctions.project.create();
            this.closeModalSave();
        } else {
            this.setState({modalSave: {...this.state.modalSave, isOpen: true}});
        }
    };

    closeModalSave = () => this.setState({modalSave: {isOpen: false, content: ''}});

    saveProject = () => {
        this.props.menuFunctions.project.create(this.state.modalSave.content);
        this.closeModalSave();
    };

    openProject = () => {
        this.props.menuFunctions.project.list()
            .then((data) => {
                this.setState({
                    modalLoad: {
                        content: data,
                        ...this.state.modalLoad
                    }
                });
            });
        this.setState({
                modalLoad: {
                    title: 'Open project'
                }
            },
            this.openModal);
    };

    setWindows = () => {
        let windows = [];
        $('[data-window]').each((ii, e) => {
            windows.push({
                id: $(e).attr('id'),
                name: $(e).attr('data-title'),
                isOpen: !$(e).hasClass('closed')
            })
        });

        this.setState({
            windows: windows
        })
    };

    toggleThisWindow = () => {
        this.setState({isOpenWindow: !this.state.isOpenWindow});
    };

    hoverWindow = () => {
        this.setState({
            isHoverWindow: !this.state.isHoverWindow
        });
        document.getElementById('main-menu').onmouseleave = () => {
            this.setState({
                isHoverWindow: false
            });
            document.getElementById('main-menu').onmouseleave = null;
        }
    };

    openWindow = () => {
        this.setState({
            timeout: setTimeout(this.hoverWindow, 400)
        });
    };

    destroyTimeout = () => {
        if (this.state.timeout) {
            clearTimeout(this.state.timeout)
        }
    };

    onClickWindow = (window) => {
        let $e = $(`#${window.id}`);
        if (window.isOpen) {
            $e.addClass('closed');
        } else {
            $e.removeClass('closed');
        }
        if ($(`#${window.id}`).hasClass('left')) {
            if ($('.window.left:not(.closed)').length > 0)
                $('.workplace').css('left', '252px');
        } else if ($(`#${window.id}`).hasClass('right')) {
            if ($('.window.right:not(.closed)').length > 0)
                $('.workplace').css('right', '252px');
        }
    };

    render() {
        let content = '';
        if (this.state.modalLoad.content && this.state.modalLoad.content.length > 0) {
            content = this.state.modalLoad.content.map((item) => {
                return <div key={item.id} className="d-row">
                    <div className="d-cell align-center">{item.name}</div>
                    <div className="d-cell align-center">{moment(item.date).format('DD.MM.YYYY')}</div>
                    <div className="d-cell align-right padding_right">
                        <button
                            onClick={this.props.menuFunctions.project.open.bind(this, item.id, this.closeModal)}
                            className="btn btn-img type_open-folder"></button>
                        <button
                            onClick={this.props.menuFunctions.project.remove.bind(this, item.id, this.closeModal)}
                            className="btn btn-img type_close"></button>

                    </div>
                </div>
            });
        }
        return (
            <div id="main-tab-wrapper"
                 className={(this.state.isOpenWindow || this.state.isHoverWindow ? '' : ' closed')}>
                {
                    !this.state.isOpenWindow
                        ? <div className="hover-menu" onMouseEnter={this.openWindow}
                               onMouseLeave={this.destroyTimeout}></div>
                        : ''
                }
                <div id="main-menu"
                     className={'main-menu-tab' + (this.state.isOpenWindow || this.state.isHoverWindow ? '' : ' closed')}>
                    <Dropdown title="File">
                        <div onClick={this.props.menuFunctions.project.new.bind(this)}>New project</div>
                        <div onClick={this.openProject}>Open project</div>
                        <div onClick={this.openModalSave}>Save project</div>
                    </Dropdown>
                    <Dropdown disabled={true} title="Edit" menuStyle={{width: '150px'}}/>
                    <Dropdown disabled={true} title="View" menuStyle={{height: '300px', width: '150px'}}/>
                    <Dropdown disabled={true} title="Navigate" menuStyle={{height: '300px', width: '150px'}}/>
                    <Dropdown title="Window"
                              preOpenFunc={this.setWindows}
                        // onClickItem={}
                    >
                        {
                            this.state.windows.map((w) => {
                                return (
                                    <div key={w.id}
                                         onClick={this.onClickWindow.bind(this, w)}
                                         className={'tab_windows' + (w.isOpen ? ' opened' : ' closed')}>{w.name}</div>
                                )
                            })
                        }
                    </Dropdown>

                    <span className="right-element">
                    <button className={'btn-round' +
                    (this.state.isOpenWindow ? ' type_caret-top' : ' type_caret-down')}
                            onClick={this.toggleThisWindow}></button>
                </span>
                </div>
                <Modal
                    isOpen={this.state.modalLoad.isOpen}
                    // onAfterOpen={afterOpenFn}
                    onRequestClose={this.closeModal}
                    // closeTimeoutMS={n}
                    style={{
                        overlay: {
                            position: 'fixed',
                            zIndex: 10000,
                            top: 0,
                            left: 0,
                            right: 0,
                            bottom: 0,
                            backgroundColor: 'rgba(255, 255, 255, 0.75)'
                        }
                    }}
                    className="modal"
                    contentLabel="Modal"
                >
                    <div className="modal_header">
                        <span>Open project</span>
                    </div>
                    <div className="modal_content">
                        <div className="d-table">
                            {
                                content
                            }
                        </div>
                    </div>
                </Modal>
                <Modal
                    isOpen={this.state.modalSave.isOpen}
                    // onAfterOpen={afterOpenFn}
                    onRequestClose={this.closeModalSave}
                    // closeTimeoutMS={n}
                    style={{
                        overlay: {
                            position: 'fixed',
                            zIndex: 10000,
                            top: 0,
                            left: 0,
                            right: 0,
                            bottom: 0,
                            backgroundColor: 'rgba(255, 255, 255, 0.75)'
                        }
                    }}
                    className="modal"
                    contentLabel="Modal"
                >
                    <div className="modal_header">
                        <span>Save Project</span>
                    </div>
                    <div className="modal_content">
                        <div className="d-table">
                            <input onChange={this.handleName} className="property-input"/>
                            <button disabled={!this.state.modalSave.content} onClick={this.saveProject} className="btn">
                                Сохранить
                            </button>
                        </div>
                    </div>
                </Modal>
            </div>
        )
    }
}

export default functionalHandler(MainMenu);