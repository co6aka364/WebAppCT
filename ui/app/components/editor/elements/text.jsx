import React, {Component} from "react";
import Element from './element.jsx'
import * as constants from '../../../public/dictionary/constants';

export default class Text extends Component {

    render() {
        return <Element title="Text" type={constants.elements.text}>
            <span>Text</span>
        </Element>
    }
}