import React, {Component} from "react";
import $ from 'jquery';
import Util from '../../../public/js/util';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as elementActions from '../../../redux/actions/element';
import * as constants from '../../../public/dictionary/constants';

import './element.scss'

class Element extends Component {
    constructor(props) {
        super(props);
        this.state = {
            guid: Util.guid()
        };
    }

    onRemoveElement = () => {
        this.props.elementActions.removeElement(this.props.selectedElement);
        this.props.elementActions.rebuildTree();
    };

    onWrapperClick = () => {
        let $e = $($.clone($('#' + this.state.guid)[0]));
        $e.addClass('draggable-element');
        // $e.addClass('resizable-element');

        if (this.props.type === constants.elements.block) {
            $e.addClass('element-drop-zone');
        }

        $e.attr('id', Util.guid());
        $e.attr('data-wact-element', '');
        let $parent = $($.find('[data-grandparent="true"]')[0]);
        $e.attr('data-parent', $parent.attr('id'));
        let length = $.find('[data-name*="' + this.props.title + '"]').length;
        $e.attr('data-name', length === 0 ? this.props.title : this.props.title + '-' + length);
        $e.find('.element-over')[0].onclick = (e) => {
            this.props.elementActions.selectElement($(e.toElement).parent().attr('id'));
        };
        $e.appendTo($parent);
        this.props.elementActions.rebuildTree();
    };

    render() {
        let selectedMenu = null;
        if (this.props.selectedElement) {
            let $e = $(`#${this.props.selectedElement.id}`);
            let attr = $e.attr('data-selected');
            if (attr && attr.toString() === 'true' && $e.attr('data-grandparent') !== 'true') {
                let $element = $($e.children()[0]);
                $element.css('display', 'block');
                $element.css('z-index', '1000');
                $element.first('button').click(this.onRemoveElement);
            } else {
                $e.find('[data-element-menu]').css('display', 'none');
            }
        }
        return <div id={this.state.guid}
                    className="element-wrapper"
                    data-element-type={this.props.type}
        >
            <div data-element-menu={true} style={{
                display: 'none',
                position: 'absolute',
                background: 'linear-gradient(to left, #252e3d, #606384)',
                borderRadius: '5px',
                width: '40%',
                right: '0',
                top: '-30px',
                minWidth: '25px'
            }}>
                <span style={{float: 'right'}}>
                    <button className="btn btn-img small type_close"></button>
                </span>
            </div>
            <div title={this.props.title}
                 onClick={this.onWrapperClick}
                 className="element-over"></div>
            {this.props.children}
        </div>;
    }
}

Element.propTypes = {
    title: React.PropTypes.string,
    type: React.PropTypes.string.isRequired
};

function mapStateToProps(state) {
    return {
        selectedElement: state.element.selected
    }
}

function mapDispatchToProps(dispatch) {
    return {
        elementActions: bindActionCreators(elementActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Element);