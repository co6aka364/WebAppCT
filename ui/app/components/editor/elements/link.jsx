import React, {Component} from "react";
import Element from './element.jsx'
import * as constants from '../../../public/dictionary/constants';

export default class Link extends Component {

    render() {
        return <Element title="Link" type={constants.elements.link}>
            <a>Link</a>
        </Element>
    }
}