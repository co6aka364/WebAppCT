import React, {Component} from "react";
import Element from './element.jsx'
import * as constants from '../../../public/dictionary/constants';

export default class FileUpload extends Component {

    render() {
        return <Element title="File Upload" type={constants.elements.fileUpload}>
            <input type="file"
                   style={{
                       border: '1px solid #000',
                       padding: '0',
                       width: '170px',
                       background: '#777'
                   }}
            />
        </Element>
    }
}