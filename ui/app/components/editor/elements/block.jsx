import React, {Component} from "react";
import Element from './element.jsx'
import * as constants from '../../../public/dictionary/constants';

export default class Block extends Component {

    render() {
        return <Element title="Block" type={constants.elements.block}>
            <div style={{
                height: '50px',
                width: '170px',
                border: '1px solid #000',
                background: '#fff'
            }}></div>
        </Element>
    }
}