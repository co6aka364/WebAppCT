import React, {Component} from "react";
import Element from './element.jsx'
import * as constants from '../../../public/dictionary/constants';

export default class Select extends Component {

    render() {
        return <Element title="Select" type={constants.elements.select}>
            <select
                style={{
                    border: '1px solid #000',
                    padding: '0',
                    width: '170px'
                }}
            ></select>
        </Element>
    }
}