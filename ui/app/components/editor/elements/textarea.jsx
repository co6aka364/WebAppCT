import React, {Component} from "react";
import Element from './element.jsx'
import * as constants from '../../../public/dictionary/constants';

export default class TextArea extends Component {

    render() {
        return <Element title="Text Area" type={constants.elements.textarea}>
            <textarea style={{
                border: '1px solid #000',
                padding: '0',
                width: '170px'
            }}>

            </textarea>
        </Element>
    }
}