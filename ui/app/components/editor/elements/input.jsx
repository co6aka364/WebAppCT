import React, {Component} from "react";
import Element from './element.jsx'
import * as constants from '../../../public/dictionary/constants';

export default class Input extends Component {

    render() {
        return <Element title="Input" type={constants.elements.input}>
            <input type="text"
                   style={{
                       border: '1px solid #000',
                       padding: '0'
                   }}
            />
        </Element>
    }
}