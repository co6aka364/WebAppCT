import React, {Component} from "react";
import moment from 'moment';
import functionalHandler from '../hoc/functional-handlers.jsx';
import Modal from 'react-modal';
import $ from 'jquery';

import "./tool-menu.scss";

class ToolMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isModalOpen: false,
            modalLoad: {
                isOpen: false,
                title: '',
                content: []
            },
            modalSave: {
                isOpen: false,
                content: ''
            }
        }
    }

    handleName = (event) => {
        this.setState({modalSave: {...this.state.modalSave, content: event.target.value}})
    };

    openModal = () => {
        this.setState({modalLoad: {...this.state.modalLoad, isOpen: true}});
    };

    closeModal = () => this.setState({modalLoad: {isOpen: false, content: []}});

    openModalSave = () => {
        let $e = $('.workplace');
        let projectId = $e.attr('data-project-id');
        let projectName = $e.attr('data-project-name');

        if (projectName && projectId) {
            this.props.menuFunctions.project.create();
            this.closeModalSave();
        } else {
            this.setState({modalSave: {...this.state.modalSave, isOpen: true}});
        }
    };

    closeModalSave = () => this.setState({modalSave: {isOpen: false, content: ''}});

    saveProject = () => {
        this.props.menuFunctions.project.create(this.state.modalSave.content);
        this.closeModalSave();
    };

    openProject = () => {
        this.props.menuFunctions.project.list()
            .then((data) => {
                this.setState({
                    modalLoad: {
                        content: data,
                        ...this.state.modalLoad
                    }
                });
            });
        this.setState({
                modalLoad: {
                    title: 'Open project'
                }
            },
            this.openModal);
    };

    render() {
        let content = '';
        if (this.state.modalLoad.content && this.state.modalLoad.content.length > 0) {
            content = this.state.modalLoad.content.map((item) => {
                return <div key={item.id} className="d-row">
                    <div className="d-cell align-center">{item.name}</div>
                    <div className="d-cell align-center">{moment(item.date).format('DD.MM.YYYY')}</div>
                    <div className="d-cell align-right padding_right">
                        <button
                            onClick={this.props.menuFunctions.project.open.bind(this, item.id, this.closeModal)}
                            className="btn btn-img type_open-folder"></button>
                        <button
                            onClick={this.props.menuFunctions.project.remove.bind(this, item.id, this.closeModal)}
                            className="btn btn-img type_close"></button>

                    </div>
                </div>
            });
        }
        return (
            <div className="tool-menu-tab">
                <button onClick={this.props.menuFunctions.project.new.bind(this)}
                        className="btn btn-img type_new-file"></button>
                <button onClick={this.openProject} className="btn btn-img type_open-folder"></button>
                <button onClick={this.openModalSave} className="btn btn-img type_save"></button>
                {/*<span className="type_separator"></span>*/}
                {/*<button disabled={true} className="btn btn-img type_backward"></button>*/}
                {/*<button disabled={true} className="btn btn-img type_forward"></button>*/}
                {/*<span className="type_separator"></span>*/}
                {/*<button disabled={true} className="btn btn-img type_scissor"></button>*/}
                {/*<button disabled={true} className="btn btn-img type_copy"></button>*/}
                {/*<button disabled={true} className="btn btn-img type_paste"></button>*/}

                <span style={{float: 'right', lineHeight: '30px', paddingRight: '20px'}}>
                    <input className="property-input" style={{width: '40px'}}
                           onChange={this.props.menuFunctions.scale.changeScale.bind(this)}
                           value={this.props.menuFunctions.scale.scale}/>
                    <span> %</span>
                </span>
                <Modal
                    isOpen={this.state.modalLoad.isOpen}
                    // onAfterOpen={afterOpenFn}
                    onRequestClose={this.closeModal}
                    // closeTimeoutMS={n}
                    style={{
                        overlay: {
                            position: 'fixed',
                            zIndex: 10000,
                            top: 0,
                            left: 0,
                            right: 0,
                            bottom: 0,
                            backgroundColor: 'rgba(255, 255, 255, 0.75)'
                        }
                    }}
                    className="modal"
                    contentLabel="Modal"
                >
                    <div className="modal_header">
                        <span>Open project</span>
                    </div>
                    <div className="modal_content">
                        <div className="d-table">
                            {
                                content
                            }
                        </div>
                    </div>
                </Modal>
                <Modal
                    isOpen={this.state.modalSave.isOpen}
                    // onAfterOpen={afterOpenFn}
                    onRequestClose={this.closeModalSave}
                    // closeTimeoutMS={n}
                    style={{
                        overlay: {
                            position: 'fixed',
                            zIndex: 10000,
                            top: 0,
                            left: 0,
                            right: 0,
                            bottom: 0,
                            backgroundColor: 'rgba(255, 255, 255, 0.75)'
                        }
                    }}
                    className="modal"
                    contentLabel="Modal"
                >
                    <div className="modal_header">
                        <span>Save Project</span>
                    </div>
                    <div className="modal_content">
                        <div className="d-table">
                            <input onChange={this.handleName} className="property-input"/>
                            <button disabled={!this.state.modalSave.content} onClick={this.saveProject} className="btn">
                                Сохранить
                            </button>
                        </div>
                    </div>
                </Modal>
            </div>
        );
    }
}

export default functionalHandler(ToolMenu);