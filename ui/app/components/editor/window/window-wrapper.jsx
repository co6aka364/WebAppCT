import React from 'react';

function genericWindow(Component) {
    class WrapperWindow extends React.Component {

        // TODO: resize, drag & drop, close,

        render() {
            return <Component {...this.props} />
        }
    }
    WrapperWindow.displayName = `WrapperWindow(${Component.displayName || Component.name || 'Component'})`;

    return WrapperWindow;

}

export default genericWindow;