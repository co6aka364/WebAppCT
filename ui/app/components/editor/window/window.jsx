import React from 'react';
import windowWrapper from './window-wrapper.jsx';
import interact from 'interactjs';
import $ from 'jquery';

import './window.scss';

class Window extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    mouseDown = () => {
        let $window = $('#' + this.props.windowId);
        $window.attr('data-x', 0);
        $window.attr('data-y', $window.offset().top - 64); // TODO: 64 -> dynamic
    };

    closeWindow = () => {
        $('#' + this.props.windowId).addClass('closed');
        if ($(`#${this.props.windowId}`).hasClass('left')) {
            if ($('.window.left:not(.closed)').length === 0)
                $('.workplace').css('left', '0');
        } else if ($(`#${this.props.windowId}`).hasClass('right')) {
            if ($('.window.right:not(.closed)').length === 0)
                $('.workplace').css('right', '0');
        }
    };

    render() {
        return (
            //TODO : rewrite height with redux params
            <div id={this.props.windowId} style={
                {}}
                 className={'window ' + (this.props.positionX === 'right' ? 'right' : 'left')
                 + (this.props.positionY === 'top' ? ' top' : this.props.positionY === 'bottom' ? ' bottom' : '')
                 + (this.props.draggable ? ' draggable' : '')
                 + (this.props.resizable ? ' resizable' : '')
                 }
                 data-window={true}
                 data-title={this.props.title}
            >
                <div className="window-header">
                  <span className="window-header__title">
                      {this.props.title}
                  </span>
                    <span className="window-header__buttons">
                      <span className="movement" onMouseDown={this.mouseDown}
                            onMouseUp={this.mouseUp}>{/*&#9638;*/}</span>
                      <span className="close-window" onClick={this.closeWindow}>{/*&times;*/}</span>
                  </span>
                </div>
                <div className="window__content"
                     style={{
                         height: ($('#' + this.props.windowId).height() - 64) + 'px'
                     }}>
                    {this.props.children}
                </div>

            </div>
        );
    }
}

Window.defaultProps = {
    draggable: false,
    resizable: false
};

Window.propTypes = {
    title: React.PropTypes.string.isRequired,
    positionX: React.PropTypes.string.isRequired,
    windowId: React.PropTypes.string.isRequired,
    draggable: React.PropTypes.bool,
    resizable: React.PropTypes.bool
};

export default Window;