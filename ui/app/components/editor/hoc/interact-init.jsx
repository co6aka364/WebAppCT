import React, {Component} from "react";
import interact from 'interactjs';
import $ from 'jquery';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as elementActions from '../../../redux/actions/element';

interact.dynamicDrop(true);

function interactInit(Component) {
    class InteractWrapper extends Component {
        constructor(props) {
            super(props);
        }

        initDropZone = () => {
            interact('.window-drop-zone').dropzone({
                accept: '.window',
                // overlap: 0.5,
                ondropactivate: function (event) {
                    // add active dropzone feedback
                    event.target.classList.add('active');
                    event.relatedTarget.classList.add('drag-out');
                },
                ondragenter: function (event) {
                    event.target.classList.add('target');
                    event.relatedTarget.classList.remove('drag-out');
                },
                ondragleave: function (event) {
                    event.target.classList.remove('target');
                    event.relatedTarget.classList.add('drag-out');
                },
                ondrop: function (event) {
                    let $window = $(event.relatedTarget);
                    // console.log($(event.target).attr('id'));
                    // $window.removeClass('draggable');
                    $window.removeAttr('style');
                    switch ($(event.target).attr('id')) {
                        case 'window-left':
                            $window.removeClass('right');
                            $window.addClass('left');
                            $window.removeClass('top');
                            $window.removeClass('bottom');
                            break;
                        case 'window-left-top':
                            $window.removeClass('right');
                            $window.addClass('left');
                            $window.removeClass('bottom');
                            $window.addClass('top');
                            break;
                        case 'window-left-bottom':
                            $window.removeClass('right');
                            $window.addClass('left');
                            $window.removeClass('top');
                            $window.addClass('bottom');
                            break;
                        case 'window-right':
                            $window.removeClass('left');
                            $window.addClass('right');
                            $window.removeClass('top');
                            $window.removeClass('bottom');
                            break;
                        case 'window-right-top':
                            $window.removeClass('bottom');
                            $window.addClass('top');
                            $window.removeClass('left');
                            $window.addClass('right');
                            break;
                        case 'window-right-bottom':
                            $window.removeClass('top');
                            $window.addClass('bottom');
                            $window.removeClass('left');
                            $window.addClass('right');
                            break;
                        default:
                    }
                },
                ondropdeactivate: function (event) {
                    // remove active dropzone feedback
                    event.target.classList.remove('active');
                    event.target.classList.remove('target');
                }
            });
        };

        initElementDropZone = () => {
            interact('.element-drop-zone').dropzone({
                accept: '.draggable-element',
                // overlap: 0.5,
                ondropactivate: function (event) {
                    // add active dropzone feedback
                    // event.target.classList.add('active');
                    event.relatedTarget.classList.add('active-element');
                },
                ondragenter: function (event) {
                    // event.relatedTarget.classList.add('dz-active');
                    // event.target.classList.add('dz-active');
                    if ($(event.target).attr('data-grandparent')) {
                        $(event.target).css('box-shadow', 'inset 0 0 30px rgba(0, 0, 0, 0.4)');
                    } else {
                        $($(event.target).children()[0]).css('box-shadow', 'inset 0 0 30px rgba(0, 0, 0, 0.4)');
                    }
                },
                ondragleave: function (event) {
                    // event.target.classList.remove('dz-active');
                    if ($(event.target).attr('data-grandparent')) {
                        $(event.target).css('box-shadow', 'initial');
                    } else {
                        $($(event.target).children()[0]).css('box-shadow', 'initial');
                    }
                },
                ondrop: (event) => {
                    let $e = $(event.relatedTarget);
                    let $d = $(event.target);

                    if ($(event.target).attr('data-grandparent')) {
                        $(event.target).css('box-shadow', 'initial');
                    } else {
                        $($(event.target).children()[0]).css('box-shadow', 'initial');
                    }

                    let getAbsoluteValue = (element, axis) => {
                        if (!element.attr('data-parent')) {
                            return 0;
                        }
                        let $parent = $(`#${element.attr('data-parent')}`);
                        return getAbsoluteValue($parent, axis) + parseInt(element.attr(`data-${axis}`));
                    };
                    let getAbsoluteFunc = (e, f) => {
                        if (!e.attr('data-parent')) {
                            return 0;
                        }
                        let $parent = $(`#${e.attr('data-parent')}`);
                        return getAbsoluteFunc($parent, f) + parseInt(typeof e[f] === 'function' ? e[f]() : 0);
                    };

                    if ($e.attr('data-parent') !== $d.attr('id')) {
                        let $parent = $('#' + $e.attr('data-parent'));
                        let x;
                        let y;
                        if ($d.attr('data-grandparent')) {
                            x = (getAbsoluteValue($e, 'x'));
                            y = (getAbsoluteValue($e, 'y') + getAbsoluteFunc($parent, 'height'));
                        } else if ($parent.attr('data-grandparent')) {
                            x = (getAbsoluteValue($e, 'x') - getAbsoluteValue($d, 'x'));
                            y = (getAbsoluteValue($e, 'y') - getAbsoluteValue($d, 'y') - getAbsoluteFunc($d, 'height'));
                        } else {
                            x = (getAbsoluteValue($e, 'x') - getAbsoluteValue($d, 'x'));
                            y = (getAbsoluteValue($e, 'y') - getAbsoluteValue($d, 'y')
                            - getAbsoluteFunc($d, 'height') + getAbsoluteFunc($parent, 'height'));
                        }

                        $e.attr('style', 'transform: translate(' + x + 'px, ' + y + 'px);');
                        $e.attr('data-x', x);
                        $e.attr('data-y', y);
                        $e.attr('data-parent', $d.attr('id'));
                        let element = $e.detach();
                        $d.append(element);

                        this.props.elementActions.rebuildTree();
                    }
                },
                ondropdeactivate: function (event) {
                    // remove active dropzone feedback
                    event.target.classList.remove('active');
                    event.target.classList.remove('dz-active');
                    event.relatedTarget.classList.remove('active-element');
                }
            });
        };

        initDraggable = () => {
            let $this = this;
            interact('.window.draggable', {allowFrom: '.movement'}).draggable({
                // accept: '.movement',
                inertia: true,
                autoScroll: false,
                onstart: function (event) {
                },
                // call this function on every dragmove event
                onmove: function (event) {
                    let target = event.target,
                        // keep the dragged positionX in the data-x/data-y attributes
                        x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
                        y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

                    // translate the element
                    target.style.webkitTransform =
                        target.style.transform =
                            'translate(' + x + 'px, ' + y + 'px)';

                    // update the posiion attributes
                    target.setAttribute('data-x', x);
                    target.setAttribute('data-y', y);
                },
                // call this function on every dragend event
                onend: function (event) {
                    let $window = $(event.target);
                    if ($window.hasClass('drag-out')) {
                        $window.removeClass('drag-out');
                        $window.addClass('closed');
                        $window.css('transform', 'translate(0px, 0px)');
                    }
                }
            });

            interact('.draggable-element').draggable({
                inertia: false,
                autoScroll: false,
                restrict: {
                    restriction: ".workplace-container",
                    endOnly: true,
                    elementRect: {top: 0, left: 0, bottom: 1, right: 1}
                },
                onstart: function (event) {
                },
                // call this function on every dragmove event
                onmove: function (event) {
                    let target = event.target,
                        // keep the dragged positionX in the data-x/data-y attributes
                        x = (parseFloat(target.getAttribute('data-x')) || 0) + ((event.dx / $this.props.scale)),
                        y = (parseFloat(target.getAttribute('data-y')) || 0) + ((event.dy / $this.props.scale));

                    // translate the element
                    target.style.webkitTransform =
                        target.style.transform =
                            'translate(' + x + 'px, ' + y + 'px)';

                    // update the posiion attributes
                    target.setAttribute('data-x', x);
                    target.setAttribute('data-y', y);
                },
                // call this function on every dragend event
                onend: function (event) {
                }
            })
        };

        initResizable = () => {
            interact('.window.resizable')
                .resizable({
                    // preserveAspectRatio: true,
                    edges: {left: true, right: true/*, bottom: true, top: true*/}
                })
                .on('resizemove', function (event) {
                    let target = event.target;
                    let x = (parseFloat(target.getAttribute('data-x')) || 0);
                    let y = (parseFloat(target.getAttribute('data-y')) || 0);

                    $(target).css('height', '100%');
                    $(target).css('width', '40%');
                    let maxHeight = window.getComputedStyle(document.getElementById($(target).attr('id')), null)["height"].replace('px', '');
                    let maxWidth = window.getComputedStyle(document.getElementById($(target).attr('id')), null)["width"].replace('px', '');

                    // update the element's style
                    target.style.width = (event.rect.width < 175 ? 175 : event.rect.width > maxWidth ? maxWidth : event.rect.width) + 'px';
                    target.style.height = (event.rect.height < 150 ? 150 : event.rect.height > maxHeight ? maxHeight : event.rect.height) + 'px';

                    // translate when resizing from top or left edges
                    x += event.deltaRect.left;
                    y += event.deltaRect.top;

                    // if ($(target).hasClass('bottom')) {
                    //     target.style.marginTop = (maxHeight - event.rect.height) + 'px';
                    // }

                    target.setAttribute('data-x', x);
                    target.setAttribute('data-y', y);
                });

            interact('.resizable-element')
                .resizable({
                    // restrict: {
                    //   restriction: 'parent'
                    // },
                    // preserveAspectRatio: true,
                    edges: {left: true, right: true, bottom: true, top: true}
                })
                .on('resizemove', function (event) {
                    let target = event.target;
                    let x = (parseFloat(target.getAttribute('data-x')) || 0);
                    let y = (parseFloat(target.getAttribute('data-y')) || 0);

                    x += event.deltaRect.left;
                    y += event.deltaRect.top;

                    target.setAttribute('data-x', x);
                    target.setAttribute('data-y', y);
                });
        };

        render() {
            this.initDropZone();
            this.initElementDropZone();
            this.initResizable();
            this.initDraggable();
            return <Component {...this.props} />;
        }
    }
    function mapStateToProps(state) {
        return {
            scale: state.element.scale
        }
    }

    function mapDispatchToProps(dispatch) {
        return {
            elementActions: bindActionCreators(elementActions, dispatch)
        }
    }

    InteractWrapper.displayName = `InteractWrapper(${Component.displayName || Component.name || 'Component'})`;
    return connect(mapStateToProps, mapDispatchToProps)(InteractWrapper);
}
export default interactInit;