import React, {Component} from 'react';
import $ from 'jquery';
import _ from 'underscore';
import localForage from 'localforage';
import md5 from 'js-md5';
import lsh from '../../../public/js/local-storage-handler';
import Util from '../../../public/js/util';
import {stores} from '../../../public/dictionary/constants';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as elementActions from '../../../redux/actions/element';

localForage.__proto__.setStore = (store) => {
    localForage.config({storeName: store});
    return localForage;
};

function functionalHandlers(Component) {
    class FunctionalHandler extends Component {
        constructor(props) {
            super(props);
            this.state = {
                scaleValue: 100,
                modalLoadProject: {
                    isOpen: false,
                    content: []
                },
                modalSaveProject: {
                    isOpen: false,
                    name: ''
                }
            };
        }

        saveProject = (name, _saveAs = false) => {
            let project;
            $('[data-selected=true]').attr('data-selected', '');
            let $e = $('.workplace');
            let projectId = $e.attr('data-project-id');
            let projectName = $e.attr('data-project-name');
            let pages = $e.html();
            if (!_saveAs) {
                project = {
                    id: projectId ? projectId : Util.guid(),
                    html: pages,
                    hash: md5(pages),
                    date: new Date(),
                    name: projectName ? projectName : name
                };
            } else {
                project = {
                    id: Util.guid(),
                    html: pages,
                    hash: md5(pages),
                    date: new Date(),
                    name: name
                };
            }

            localForage.setStore(stores.projects)
                .setItem(project.id, project)
                .then(() => {
                    $e.attr('data-project-id', project.id);
                    $e.attr('data-project-name', project.name);
                })
                .then((error) => {

                });
        };

        openProject = (id, callback) => {
            localForage.getItem(id).then((project) => {
                if (project && project.id && project.html) {

                    // TODO: реализовать для скписка страниц
                    $('.workplace-container[data-grandparent]').replaceWith(project.html);
                    $('.workplace-container[data-grandparent] .element-over').click((e) => {
                        this.props.elementActions.selectElement($(e.toElement).parent().attr('id'));
                    });
                    this.props.elementActions.rebuildTree();
                    this.props.elementActions.selectElement(null);

                    let $e = $('.workplace');
                    $e.attr('data-project-id', project.id);
                    $e.attr('data-project-name', project.name ? project.name : null);
                }
                if (callback && typeof callback === 'function') {
                    callback();
                }
            });
        };

        getProjectList = () => {
            return localForage.setStore(stores.projects)
                .keys()
                .then((keys) => {
                    let promises = [];
                    for (let key of keys) {
                        promises.push(localForage.setStore(stores.projects).getItem(key))
                    }
                    return Promise.all(promises);
                });
        };

        createNewProject = (name, width = 200, height = 800) => {
            name = typeof name === 'object' ? '' : name;
            let $p = $('.workplace');
            $p.html(
                `<div
                    data-wact-element
                    id=${Util.guid()}
                    class="workplace-container element-drop-zone"
                    data-grandparent="true"
                    data-name="New Page"
                ></div>`
            );

            // $p.attr('data-project-id', Util.guid());
            // $p.attr('data-project-name', name ? name : null);
            $p.attr('data-project-id', '');
            $p.attr('data-project-name', '');

            let $e = $('.workplace-container[data-grandparent]');
            $e.css('height', height + 'px');
            $e.css('width', width + 'px');


            this.props.elementActions.rebuildTree();
            this.props.elementActions.selectElement(null);

        };

        removeProject = (id, callback) => {
            localForage.setStore(stores.projects)
                .removeItem(id);
            if (callback && typeof callback === 'function') {
                callback();
            }
        };

        openModal = (type = null) => {
            switch (type) {
                case 'loadProject':
                    this.getProjectList().then((data) => {
                        this.setState({modalSaveProject: {content: data, ...this.state.modalSaveProject}})
                    });
                    this.setState({modalLoadProject: {isOpen: true, ...this.state.modalLoadProject}});
                    break;
                case 'saveProject':
                    this.setState({modalSaveProject: {isOpen: true, ...this.state.modalSaveProject}});
                    break;
                default:
                    break;
            }
        };

        closeModal = (type = null) => {
            switch (type) {
                case 'loadProject':
                    this.setState({modalLoadProject: {isOpen: true, content: []}});
                    break;
                case 'saveProject':
                    this.setState({modalSaveProject: {isOpen: true, ...this.state.modalSaveProject}});
                    break;
                default:
                    break;
            }
        };

        modalLoadProject = () => {
            let content = '';
            if (this.state.modalLoad.content && this.state.modalLoad.content.length > 0) {
                content = this.state.modalLoad.content.map((item) => {
                    return <div key={item.id} className="d-row">
                        <div className="d-cell align-center">{item.name}</div>
                        <div className="d-cell align-center">{moment(item.date).format('DD.MM.YYYY')}</div>
                        <div className="d-cell align-right padding_right">
                            <button
                                onClick={this.props.openProject.bind(this, item.id, this.closeModal)}
                                className="btn btn-img type_open-folder"></button>
                            <button
                                onClick={this.props.removeProject.bind(this, item.id, this.closeModal)}
                                className="btn btn-img type_close"></button>

                        </div>
                    </div>
                });
            }
            return (<Modal
                isOpen={this.state.modalLoadProject.isOpen}
                // onAfterOpen={afterOpenFn}
                onRequestClose={this.closeModal.bind(this, 'loadProject')}
                // closeTimeoutMS={n}
                style={{
                    overlay: {
                        position: 'fixed',
                        zIndex: 10000,
                        top: 0,
                        left: 0,
                        right: 0,
                        bottom: 0,
                        backgroundColor: 'rgba(255, 255, 255, 0.75)'
                    }
                }}
                className="modal"
                contentLabel="Modal"
            >
                <div className="modal_header">
                    <span>Open project</span>
                </div>
                <div className="modal_content">
                    <div className="d-table">
                        {
                            content
                        }
                    </div>
                </div>
            </Modal>);
        };

        changeScale = (event) => {
            let scale = event.target.value;
            if (event.target.value && !isNaN(event.target.value) && event.target.value >= 0) {
                scale = event.target.value;
            } else {
                scale = 100;
            }
            this.setState({
                ...this.state, scaleValue: scale
            });
            this.props.elementActions.changeScale(event.target.value);
        };

        render() {
            let menuFuncs = {
                scale: {
                    changeScale: this.changeScale,
                    scale: this.state.scaleValue
                },
                project: {
                    new: this.createNewProject,
                    create: this.saveProject,
                    list: this.getProjectList,
                    remove: this.removeProject,
                    open: this.openProject
                }
            };
            return <Component {...this.props} menuFunctions={menuFuncs}/>
        }
    }
    FunctionalHandler.displayName = `FunctionalHandler(${Component.displayName || Component.name || 'Component'})`;

    function mapStateToProps(state) {
        return {}
    }

    function mapDispatchToProps(dispatch) {
        return {
            elementActions: bindActionCreators(elementActions, dispatch)
        }
    }

    return connect(mapStateToProps, mapDispatchToProps)(FunctionalHandler);
}

export default functionalHandlers;