import React, {Component} from "react";
import Window from '../window/window.jsx';
import "./toolbox-window.scss";

import Input from '../elements/input.jsx';
import Block from '../elements/block.jsx';
import Select from '../elements/select.jsx';
import Text from '../elements/text.jsx';
import Link from '../elements/link.jsx';
import TextArea from '../elements/textarea.jsx';
import FileUpload from '../elements/fileupload.jsx';

export default class ToolBoxWindow extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return (
            <Window title="ToolBox"
                    positionX="left"
                    windowId="toolbox-window"
                    resizable={false}
                    draggable={true}
            >
                <Block/>
                <Input/>
                <Select/>
                <FileUpload/>
                <TextArea/>
                <Text/>
                <Link/>
            </Window>
        )
    }
}