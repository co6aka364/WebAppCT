import React, {Component} from "react";
import Window from '../window/window.jsx';
import $ from 'jquery';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as elementActions from '../../../redux/actions/element';

import Tree from '../tree/tree.jsx';
import "./project-window.scss";

class ProjectWindow extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    onNodeClick = (id) => {
        this.props.elementActions.selectElement(id);
    };

    onNodeEnter = (id) => {
        $(`div[id="${id}"]`).addClass('on-mouse-enter');
        // $e.css('box-shadow', '0 0 100px 20px rgba(0, 0, 0, 0.3) inset');
    };

    onNodeLeave = (id) => {
        $(`div[id="${id}"]`).removeClass('on-mouse-enter');
        // $(`div[id="${id}"]`).css('box-shadow', '');
    };

    render() {
        return (
            <Window title="Project"
                    positionX="right"
                    positionY="top"
                    windowId="project-window"
                    resizable={false}
                    draggable={true}
            >
                <Tree onNodeClick={this.onNodeClick}
                      onNodeEnter={this.onNodeEnter}
                      onNodeLeave={this.onNodeLeave}
                      nodes={this.props.elementsTree}/>
            </Window>
        )
    }
}

function mapStateToProps(state) {
    return {
        elementsTree: state.element.tree
    }
}

function mapDispatchToProps(dispatch) {
    return {
        elementActions: bindActionCreators(elementActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProjectWindow);