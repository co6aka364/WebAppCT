export const elements = {
    block: 'BLOCK',
    input: 'INPUT',
    link: 'LINK',
    text: 'TEXT',
    textarea: 'TEXTAREA',
    select: 'SELECT',
    fileUpload: 'FILEUPLOAD'
};

export const stores = {
    projects: 'projects'
};