export const common = [
    // {
    //     property: 'data-name',
    //     name: 'Name',
    //     type: 'text'
    // },
    {
        property: 'width',
        type: 'text',
        unit: 'px'
    },
    {
        property: 'height',
        type: 'text',
        unit: 'px'
    },
    {
        property: 'margin',
        type: 'text',
        unit: 'px'
    },
    {
        property: 'padding',
        type: 'text',
        unit: 'px'
    },
];

export const block = [];

export const text = [
    {
        property: 'innerText',
        name: 'Value',
        type: 'text'
    }
];

export const link = [
    {
        property: 'innerText',
        name: 'Value',
        type: 'text'
    },
    {
        property: 'href',
        name: 'Link',
        type: 'text'
    }
];

export const extended = [
    {
        property: 'backgroundColor',
        name: 'Background Color',
        type: 'text'
    },
    {
        property: 'backgroundImage',
        name: 'Background Image',
        type: 'text'
    },
    {
        property: 'backgroundOrigin',
        name: 'Background Origin',
        type: 'text'
    },
    // {
    //     property: 'backgroundPosition',
    //     name: 'Background Position',
    //     type: 'text'
    // },
    // {
    //     property: 'backgroundPositionX',
    //     name: 'Background PositionX',
    //     type: 'text'
    // },
    // {
    //     property: 'backgroundPositionY',
    //     name: 'Background PositionY',
    //     type: 'text'
    // },
    {
        property: 'backgroundRepeat',
        name: 'Background Repeat',
        type: 'text'
    },
    {
        property: 'backgroundRepeatX',
        name: 'Background RepeatX',
        type: 'text'
    },
    {
        property: 'backgroundRepeatY',
        name: 'Background RepeatY',
        type: 'text'
    },
    {
        property: 'backgroundSize',
        name: 'Background Size',
        type: 'text'
    },
    {
        property: 'height',
        name: 'height',
        type: 'text'
    },
    {
        property: 'width',
        name: 'width',
        type: 'text'
    },
    {
        property: 'color',
        name: 'color',
        type: 'text'
    },
    {
        property: 'display',
        name: 'display',
        type: 'text'
    },
    {
        property: 'maxHeight',
        name: 'Max Height',
        type: 'text'
    },
    {
        property: 'maxWidth',
        name: 'Max Width',
        type: 'text'
    },
    {
        property: 'minHeight',
        name: 'Min Height',
        type: 'text'
    },
    {
        property: 'minWidth',
        name: 'Min Width',
        type: 'text'
    },
    {
        property: 'opacity',
        name: 'opacity',
        type: 'text'
    },
    {
        property: 'marginBottom',
        name: 'Margin Bottom',
        type: 'text'
    },
    {
        property: 'marginLeft',
        name: 'Margin Left',
        type: 'text'
    },
    {
        property: 'marginRight',
        name: 'Margin Right',
        type: 'text'
    },
    {
        property: 'marginTop',
        name: 'Margin Top',
        type: 'text'
    },
    {
        property: 'paddingBottom',
        name: 'Padding Bottom',
        type: 'text'
    },
    {
        property: 'paddingLeft',
        name: 'Padding Left',
        type: 'text'
    },
    {
        property: 'paddingRight',
        name: 'Padding Right',
        type: 'text'
    },
    {
        property: 'paddingTop',
        name: 'Padding Top',
        type: 'text'
    },
    {
        property: 'borderStyle',
        name: 'Border Style',
        type: 'text'
    },
    {
        property: 'borderColor',
        name: 'Border Color',
        type: 'text'
    },
    {
        property: 'borderWidth',
        name: 'Border Width',
        type: 'text'
    },
    {
        property: 'borderCollapse',
        name: 'Border Collapse',
        type: 'text'
    },
    {
        property: 'position',
        name: 'position',
        type: 'text'
    },
    {
        property: 'top',
        name: 'top',
        type: 'text'
    },
    {
        property: 'bottom',
        name: 'bottom',
        type: 'text'
    },
    {
        property: 'left',
        name: 'left',
        type: 'text'
    },
    {
        property: 'right',
        name: 'right',
        type: 'text'
    },
    {
        property: 'boxShadow',
        name: 'Box Shadow',
        type: 'text'
    },
    {
        property: 'clear',
        name: 'clear',
        type: 'text'
    },
    {
        property: 'cursor',
        name: 'cursor',
        type: 'text'
    },
    {
        property: 'float',
        name: 'float',
        type: 'text'
    },
    {
        property: 'fontFamily',
        name: 'Font Family',
        type: 'text'
    },
    {
        property: 'fontSize',
        name: 'Font Size',
        type: 'text'
    },
    {
        property: 'fontStyle',
        name: 'Font Style',
        type: 'text'
    },
    {
        property: 'fontVariant',
        name: 'Font Variant',
        type: 'text'
    },
    {
        property: 'fontWeight',
        name: 'Font Weight',
        type: 'text'
    },
    {
        property: 'justifyContent',
        name: 'Justify Content',
        type: 'text'
    },
    {
        property: 'lineHeight',
        name: 'Line Height',
        type: 'text'
    },
    {
        property: 'outline',
        name: 'outline',
        type: 'text'
    },
    {
        property: 'outlineColor',
        name: 'Outline Color',
        type: 'text'
    },
    {
        property: 'outlineOffset',
        name: 'Outline Offset',
        type: 'text'
    },
    {
        property: 'outlineStyle',
        name: 'Outline Style',
        type: 'text'
    },
    {
        property: 'outlineWidth',
        name: 'Outline Width',
        type: 'text'
    },
    {
        property: 'textAlign',
        name: 'Text Align',
        type: 'text'
    },
    {
        property: 'textDecoration',
        name: 'Text Decoration',
        type: 'text'
    },
    {
        property: 'textIndent',
        name: 'Text Indent',
        type: 'text'
    },
    {
        property: 'wordBreak',
        name: 'Word Break',
        type: 'text'
    },
    {
        property: 'wordSpacing',
        name: 'Word Spacing',
        type: 'text'
    },
    {
        property: 'wordWrap',
        name: 'Word Wrap',
        type: 'text'
    },
    {
        property: 'zIndex',
        name: 'zIndex',
        type: 'text'
    }
];