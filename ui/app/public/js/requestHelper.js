import $ from "jquery";
import auth from "./auth";

export default class Request {
    static get(url) {
        return $.ajax({
            url: '/api' + url,
            method: 'GET',
            contentType: "application/json",
            headers: {'Authorization': auth.getToken()}
        })
            .always((response, status) => {
                if (status === 'error') {
                    if (response.status === 401) {
                        auth.logout();
                        window.location = '/';
                    }
                }
            })
    }

    static put(url, data = {}) {
        return $.ajax({

            url: '/api' + url,
            method: 'PUT',
            contentType: "application/json",
            headers: {'Authorization': auth.getToken()},
            data: data
        })
            .always((response, status) => {
                if (status === 'error') {
                    if (response.status === 401) {
                        auth.logout();
                        window.location = '/';
                    }
                }
            })
    }

    static post(url, data = {}) {
        return $.ajax({
            url: '/api' + url,
            method: 'POST',
            contentType: "application/json",
            headers: {'Authorization': auth.getToken()},
            data: data
        })
            .always((response, status) => {
                if (status === 'error') {
                    if (response.status === 401) {
                        auth.logout();
                        window.location = '/';
                    }
                }
            })
    }

    static delete(url) {
        return $.ajax({
            url: '/api' + url,
            method: 'DELETE',
            contentType: "application/json",
            headers: {'Authorization': auth.getToken()}
        })
            .always((response, status) => {
                if (status === 'error') {
                    if (response.status === 401) {
                        auth.logout();
                        window.location = '/';
                    }
                }
            })
    }
}