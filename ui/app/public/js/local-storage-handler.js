export default class LocalStorageHandler {
    static setItem(key, value) {
        localStorage.setItem(key.toString(), JSON.stringify(value));
    }

    static getItem(key) {
        return JSON.parse(localStorage.getItem(key.toString()));
    }
}