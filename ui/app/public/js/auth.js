export default class AuthenticationManager {
    constructor() {

    }

    static constants = {
        token: 'JWT_TOKEN'
    };

    static logout() {
        localStorage.removeItem(AuthenticationManager.constants.token);
    }

    static saveToken = (token) => {
        localStorage.setItem(AuthenticationManager.constants.token, token);
    };

    static isHasToken() {
        return !!localStorage.getItem(AuthenticationManager.constants.token);
    }

    static getToken() {
        return localStorage.getItem(AuthenticationManager.constants.token);
    }
}
