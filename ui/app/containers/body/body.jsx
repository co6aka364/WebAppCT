// import React, {Component} from "react";
// import $ from "jquery";
// import {bindActionCreators} from 'redux';
// import {connect} from 'react-redux';
//
// import Header from '../../components/header/header.jsx'
// // import * as overlayActions from '../../redux/actions/overlay';
// // import * as clientActions from '../../redux/actions/client';
//
// import "./body.scss";
//
// class Body extends Component {
//     constructor(props) {
//         super(props);
//         this.state = {}
//     }
//
//     componentDidMount() {
//         // this.getClient();
//     }
//
//     getClient() {
//         // this.props.clientActions.get();
//     }
//
//     handleHide = () => {
//         // if (this.props.overlay.isShowOverlay && this.props.loader && !this.props.loader.isShowLoader) {
//         //     this.props.overlayActions.hideOverlay()
//         // }
//     };
//
//     render() {
//         return (
//             <div>
//                 <Header title={this.props.children.props.route.title}/>
//
//                 <div className="background-layout">
//                     <section className="wrapper">
//                         {/*<Loader />*/}
//                         {/*<div*/}
//                             {/*className={this.props.overlay.isShowOverlay || this.props.loader.isShowLoader*/}
//                                 {/*? 'blur_layout open'*/}
//                                 {/*: 'blur_layout'}*/}
//                             {/*onClick={this.handleHide}*/}
//                             {/*disabled={true}>*/}
//                             <div className="content">
//
//                                 {this.props.children}
//                             </div>
//                         {/*</div>*/}
//                     </section>
//                 </div>
//             </div>
//         );
//     }
// }
//
// function mapStateToProps(state) {
//     return {
//         // overlay: state.overlay,
//         // loader: state.loader,
//     }
// }
// function mapDispatchToProps(dispatch) {
//     return {
//         // overlayActions: bindActionCreators(overlayActions, dispatch),
//         // clientActions: bindActionCreators(clientActions, dispatch)
//     }
// }
//
// export default connect(mapStateToProps, mapDispatchToProps)(Body)