import React, {Component} from 'react';
import Util from '../../public/js/util';
import localForage from 'localforage';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

class Workplace extends Component {
    constructor(props) {
        super(props);
        this.state = {
            guid: Util.guid()
        }
    }

    componentDidMount() {
        // console.log(this.workplace);
        // localForage.setStore('test').setItem(Util.guid(), this.workplace, (err, val) => {
        //     console.log(val);
        // });
    }

    render() {
        return <div className="workplace" ref={(workplace) => this.workplace = workplace}>
            <div className="workplace-wrapper"
                 style={{padding: '30px', transform: `scale(${this.props.scale})`, transformOrigin: 'top left'}}>
                <div
                    data-wact-element
                    data-parent={null}
                    id={this.state.guid}
                    className="workplace-container element-drop-zone"
                    data-name="New Page"
                    data-grandparent="true"
                    style={{height: '400px', width: '800px'}}
                    // onClick={() => {
                    //     this.props.elementActions.selectElement(this.state.guid);
                    // }}
                >
                </div>
            </div>
        </div>
    }
}
function mapStateToProps(state) {
    return {
        scale: state.element.scale
    }
}

function mapDispatchToProps(dispatch) {
    return {}
}
export default connect(mapStateToProps, mapDispatchToProps)(Workplace);
