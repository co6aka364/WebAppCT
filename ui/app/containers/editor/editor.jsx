import React, {Component} from "react";
import interact from 'interactjs';
import $ from 'jquery';
import interactInit from '../../components/editor/hoc/interact-init.jsx'
import Util from '../../public/js/util';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as elementActions from '../../redux/actions/element';

import "./editor.scss";
/*  COMPONENTS    */
import MainMenu from "../../components/editor/main-menu/main-menu.jsx";
import ToolMenu from "../../components/editor/tool-menu/tool-menu.jsx";
import ToolBoxWindow from "../../components/editor/toolbox-window/toolbox-window.jsx";
import ProjectWindow from "../../components/editor/project-window/project-window.jsx";
import PropertyWindow from "../../components/editor/property-window/property-window.jsx";
import BottomMenu from "../../components/editor/bottom-menu/bottom-menu.jsx";
import Workplace from './editor-workplace.jsx';
/*  COMPONENTS    */

class Editor extends Component {
    constructor(props) {
        super(props);
        this.state = {
            guid: Util.guid()
        }
    }

    componentDidMount() {
        this.props.elementActions.rebuildTree();
    }

    render() {
        return (
            <div className="editor-background">
                <MainMenu ref="mainMenu"/>
                <ToolMenu />
                <div className="editor-two-menu">

                    <ToolBoxWindow />
                    <ProjectWindow />
                    <PropertyWindow />

                    <Workplace />

                    <div id="window-left" className="window-drop-zone left"></div>
                    <div id="window-left-top" className="window-drop-zone left-top"></div>
                    <div id="window-left-bottom" className="window-drop-zone left-bottom"></div>
                    <div id="window-right" className="window-drop-zone right"></div>
                    <div id="window-right-top" className="window-drop-zone right-top"></div>
                    <div id="window-right-bottom" className="window-drop-zone right-bottom"></div>
                </div>
                <BottomMenu />
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {}
}

function mapDispatchToProps(dispatch) {
    return {
        elementActions: bindActionCreators(elementActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(interactInit(Editor));