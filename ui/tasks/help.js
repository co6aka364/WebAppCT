'use strict';
const gutil = require('gulp-util');
module.exports = function (done) {
    gutil.log(
        gutil.colors.green('\n\n Gulp commands: \n\n'),
        gutil.colors.cyan('clean'), 'Очистка папки с билдом\n',
        gutil.colors.cyan('help'), 'Выводит список команд\n\n',
        gutil.colors.cyan('db:load'), 'Загружает фикстуры в БД\n',
        gutil.colors.cyan('db:start'), 'Запускает локально БД\n',
        gutil.colors.cyan('db:stop'), 'Останавливает локальную БД\n\n',
        gutil.colors.cyan('webpack:dev'), 'Собирает клиентскую часть в режиме разработки и запускает вотчер\n',
        gutil.colors.cyan('webpack:prod'), 'Собирает клиентскую часть в для продакшн\n'
    );
};