const config = require('config');
const gulp = require('gulp');
const sequence = require('gulp-sequence');
const TASKS_FOLDER = './tasks/';
gulp.task('dev', (done) => {
    sequence('clean', 'webpack:dev', () => {
        done();
    });
});
gulp.task('prod', (done) => {
    sequence('clean', 'webpack:prod', () => {
        done();
    });
});
// Common
gulp.task('clean', require(TASKS_FOLDER + 'clean'));
gulp.task('help', require(TASKS_FOLDER + 'help'));
gulp.task('default', require(TASKS_FOLDER + 'help'));
// DataBase
// gulp.task('db:load', require(TASKS_FOLDER + 'db.load'));
// gulp.task('db:start', require(TASKS_FOLDER + 'db.start'));
// gulp.task('db:stop', require(TASKS_FOLDER + 'db.stop'));
// Client
gulp.task('webpack:dev', require(TASKS_FOLDER + 'webpack.dev'));
gulp.task('webpack:prod', require(TASKS_FOLDER + 'webpack.prod'));