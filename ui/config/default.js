const path = require('path');

module.exports = {
    root: process.cwd(),
    buildFolderName: 'dist',
    buildFolder: function () {
        return path.join(this.root, this.buildFolderName);
    }
};