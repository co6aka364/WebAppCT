'use strict';

const NODE_ENV = process.env.NODE_ENV || 'develop';
const config = require('config');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    context: __dirname,
    entry: {
        index: './app/index.jsx',
        vendor: ['react', 'jquery'],
        // login: './app/login/login.jsx',
        // registration: './app/registration/registration.jsx'
    },
    output: {
        path: __dirname + '/dist',
        filename: '[name].js'
    },
    resolve: {
        modulesDirectories: ['bower_components', 'node_modules'],
        extenstions: ['']
    },
    resolveLoader: {
        modulesDirectories: ['node_modules'],
        moduleTemplates: ['*-loader'],
        extenstions: ['', '.js']
    },
    plugins: [
        new webpack.NoErrorsPlugin(),
        new HtmlWebpackPlugin({
            favicon: './app/public/images/logo.png',
            template: './app/index.html',
            showErrors: true,
            chunks: ['vendor', 'index']
        }),
        // new HtmlWebpackPlugin({
        //     favicon: './app/public/images/logo.png',
        //     filename: 'login.html',
        //     template: './app/login/login.html',
        //     chunks: ['vendor', 'login'],
        //     showErrors: true
        // }),
        // new HtmlWebpackPlugin({
        //     favicon: './app/public/images/logo.png',
        //     filename: 'registration.html',
        //     template: './app/registration/registration.html',
        //     chunks: ['vendor', 'registration'],
        //     showErrors: true
        // }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            minChunks: 4
        })
    ],
    module: {
        noParse: /node_modules\/localforage\/dist\/localforage.js/,
        loaders: [
            {
                test: /\.scss$/,
                loaders: ['style', 'css', 'sass']
            },
            {
                test: /\.css$/,
                loaders: ['style', 'css']
            },
            {
                test: /\.html$/,
                loader: 'html'
            },
            {
                test: /.*\.(jsx|js)?$/,
                loader: 'babel',
                exclude: /node_modules/,
                query: {
                    presets: ['es2015', 'stage-0', 'react']
                }
            },
            {
                test: /.*\.(gif|png|jpe?g|svg)$/i,
                loaders: [
                    'file?hash=sha512&digest=hex&name=[hash].[ext]',
                    'image-webpack?{optimizationLevel: 7, interlaced: false, pngquant:{quality: "0-100", speed: 4}, mozjpeg: {quality: 65}}'
                ]
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                loader: 'file'
            }
        ]
    }
};